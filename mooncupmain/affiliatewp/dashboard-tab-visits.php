<div id="affwp-affiliate-dashboard-visits" class="affwp-tab-content">

	<h4><?php _e( 'Your Clicks', 'affiliatewp' ); ?></h4>

	<?php
	$per_page = 30;
	$page     = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$pages    = absint( ceil( affwp_get_affiliate_visit_count( affwp_get_affiliate_id() ) / $per_page ) );
	$visits   = affiliate_wp()->visits->get_visits(
		array(
			'number'       => $per_page,
			'offset'       => $per_page * ( $page - 1 ),
			'affiliate_id' => affwp_get_affiliate_id(),
		)
	);
	?>

	<table id="affwp-affiliate-dashboard-visits" class="affwp-table">
		<thead>
			<tr>
				<th class="visit-url"><?php _e( 'URL', 'affiliatewp' ); ?></th>
				<th class="referring-url"><?php _e( 'Referring URL', 'affiliatewp' ); ?></th>
				<th class="referral-status"><?php _e( 'Converted', 'affiliatewp' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php if ( $visits ) : ?>

				<?php foreach ( $visits as $visit ) : ?>
					<tr>
						<td><?php echo $visit->url; ?></td>
						<td><?php echo ! empty( $visit->referrer ) ? $visit->referrer : __( 'Direct traffic', 'affiliatewp' ); ?></td>
						<td>
							<?php $converted = ! empty( $visit->referral_id ) ? 'yes' : 'no'; ?>
							<span class="visit-converted <?php echo esc_attr( $converted ); ?>"><i></i></span>
						</td>
					</tr>
				<?php endforeach; ?>

			<?php else : ?>

				<tr>
					<td colspan="3"><?php _e( 'You have not received any clicks yet.', 'affiliatewp' ); ?></td>
				</tr>

			<?php endif; ?>
		</tbody>
	</table>

	<?php if ( $pages > 1 ) : ?>

		<p class="affwp-pagination">
			<?php
			echo paginate_links(
				array(
					'current'      => $page,
					'total'        => $pages,
					'add_fragment' => '#affwp-affiliate-dashboard-visits',
					'add_args'     => array(
						'tab' => 'visits',
					),
				)
			);
			?>
		</p>

	<?php endif; ?>
<p></p>
</div>
