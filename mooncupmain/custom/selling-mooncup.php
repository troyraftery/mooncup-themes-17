<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Selling the Mooncup Landing Page
 */

get_header(); ?>

<section class="single-col selling-mooncup page-content primary" role="main">

	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        	<article class="container_full content_band">
	        		<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area');?>
	        		</div>
	        	</article>

	        	<div class="container_boxed container__2col container__2col--spacer content_band">
	        		<div class="container__inner">
		        		<?php the_field('block_title_left');?>
		        		<div class="block-out grey-bg">
		        			<?php the_field('block_content_left');?>
		        		</div>
	        		</div>

	        		<div class="container__inner">
		        		<?php the_field('block_title_right');?>
		        		<div class="block-out grey-bg">
		        			<?php the_field('block_content_right');?>
		        		</div>
	        		</div>

	        	</div>

	        <aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>

</section>

<?php get_footer(); ?>
